import { Trainee } from './../core/trainee.model';
import { TrainingService } from './../core/training.service';
import { Training } from './../core/training.model';
import { TrainingRequestComponent } from './training-request/training-request.component';
import { TrainingModifyComponent } from './training-modify/training-modify.component';
import { TrainingAddComponent } from './training-add/training-add.component';
import { Component, ViewChild, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { Overlay } from '@angular/cdk/overlay';
import { UserService } from '../core/user.service';

// Angular Material 2
import {  MatTableDataSource, MatDialog, MatDialogConfig, MatSnackBar } from '@angular/material';
import { MatSort } from '@angular/material/sort';
import { DataSource } from '@angular/cdk/collections';
import {animate, state, style, transition, trigger} from '@angular/animations';

import { AngularFirestore, AngularFirestoreDocument  } from '@angular/fire/firestore';
import { AngularFirestoreCollection } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import * as _ from 'lodash';

// rxjs
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Rx';
import { map, switchMap } from 'rxjs/operators';
import {TrainingDeleteComponent } from './training-delete/training-delete.component'
import { FilterCoursePipe } from './filter-course.pipe';


@Component({
  selector: 'app-training-filter',
  // template: `<app-training-modify [userRoleModfiy]="test"></app-training-modify>`,
  templateUrl: './training-filter.component.html',
  styleUrls: ['./training-filter.component.scss'],
  animations: [
  trigger('detailExpand', [
    state('collapsed', style({height: '0px', minHeight: '0', display: 'none'})),
    state('expanded', style({height: '*'})),
    transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})


// 1st class to export
export class TrainingFilterComponent implements OnInit {

  test = "Hello world!";

  userRole:string;

  current_user: Trainee;

  ///unwrapped arrays from firebase
  courses: any;
  filteredCourse: any;

  ///filter-able properties
  Name: string;
  Costs: number;
  Duration: number;
  Target_group:string;
  Category:string;
  active:string;

  isChecked:boolean;

  ///Active filter
  filters = {};

  term: string;

  trainingDetails: Training = {
    Category: '',
    Target_group: '',
    Costs: null,
    Location: '',
    Duration: null,
    Description: '',
  };

  filter = {
    field: '',
    filtervalue: ''
  };

  expandedElement: Training | null;

  displayedColumns = ['Name', 'Language', 'Location', 'Category', 'Target_group', 'Costs', 'Duration', 'Rating', 'star'];
  //displayedColumns = ['Name', 'Language', 'Location', 'Category'];
  trainingDatabase = new TrainingDatabase(this.trainingService);


  stopPropagation(event){
    event.stopPropagation();
}

  filterPipe = new FilterCoursePipe();
  dataSourceSort:any;
  pipeData:any;

  
   @ViewChild(MatSort) sort: MatSort;

  constructor(
    public trainingService: TrainingService,
    private dialog: MatDialog,
    private toastr: ToastrService,
    private overlay: Overlay,
    public afs: AngularFirestore,
    public userService: UserService,
    ) {
    this.isChecked = false;
  }

  ngOnInit() {
    // this.dataSource = new TrainingDataSource(this.trainingDatabase, this.sort);
    // this.dataSourceSort.sort = this.sort;

    this.afs.collection('/Training').snapshotChanges().pipe(
      map(changes => {
        return changes.map(a => {
          const data = a.payload.doc.data() as Training;
          data.id = a.payload.doc.id;
          return data;
        });
      })
      ).subscribe(courses => {
        this.courses = courses;
        this.applyFilters()
      })

    // this.dataSourceSort.sort = this.sort;


     this.userService.get_current_user().subscribe(
      current_user => {
        this.current_user = current_user;
        this.userRole = current_user.userRole;
      }
    );

     this.filterExact('active', "true");    

  }



    private applyFilters(){
      this.filteredCourse = _.filter(this.courses, _.conforms(this.filters));
      this.searchFunction();
      this.dataSourceSort = new MatTableDataSource(this.pipeData);
      this.dataSourceSort.sort = this.sort;

    }

    searchFunction(){
      this.pipeData = this.filterPipe.transform(this.filteredCourse, this.term);
    }

    ///filter property by equality to rule
    filterExact(property: string, rule: any){
      this.filters[property] = val => val == rule
      this.applyFilters()
    }

    ///filter properties with numeber greater than rule
    filterSmallerThan(property: string, rule: number){
      this.filters[property] = val => val <= rule
      this.applyFilters()
    }

    /// filter properties that resolve to true
    filterBoolean(property: string, rule: boolean) {
      if (!rule) this.removeFilter(property)
        else {
          this.filters[property] = val => val
          this.applyFilters()
        }
      }


      /// filter category
      filterCategory(property: string, rule: any) {
        if (!rule) this.removeFilter(property)
          else {
            this.filters[property] = val => val == rule
            this.applyFilters()
          }
        }

        /// removes filter
        removeFilter(property: string) {
          delete this.filters[property]
          this[property] = undefined
          this.applyFilters()
        }

        buttonReset(value){
          if(value === undefined ){
            return false
          } if(value === 0 || value === null) {
            return true
          } else{
            return true
          }
        }




    checkBoxHandler(option: string){
      if(option === 'toggle'){
      this.isChecked = !this.isChecked;
      if(this.isChecked === true){
        this.filterExact('active', "false"); 
      }  
      else {
        this.filterExact('active', "true"); 
      };
    }else{
      this.isChecked = false;
    };
  

    }




        /// removes filter
        removeTerm() {
          this.term = '';
          this.applyFilters()
        }

        addTraining() {
          this.openDialog('add');
        }

        modifyMessage(training) {
          this.openDialog('modify', training);
        }

        requestTraining(training) {
          this.openDialog('request', training);
        }

        deleteMessage(training) {
          this.openDialog('delete', training);
        }


  openDialog(type, training?) {
    const dialogConfig = new MatDialogConfig();
    let message = '';
    let dialogRef;
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    switch ( type) {
      case 'modify': {
        dialogConfig.data = {
          id: training.id,
          title: 'Modify training',
          training : training,
          training_id : training.id,
          userRole: this.userRole,

        };
        dialogRef = this.dialog.open(TrainingModifyComponent, {
          width: '1000px',
          //  height: '80%',
          data: dialogConfig.data,
          autoFocus: true,
        });
        message = 'Training modified';
        break;
      }
      case 'delete': {
        dialogConfig.data = {
          id: training.id,
          title: 'Delate training',
          training : training,
          training_id : training.id,
        };
        dialogRef = this.dialog.open(TrainingDeleteComponent, {
          width: '400px',
          //  height: '80%',
          data: dialogConfig.data,
          autoFocus: true,
        });
        message = 'Training deleted';
        break;
      }
      case 'add': {
        dialogConfig.data = {
          title: 'Add training',
        };
        dialogRef = this.dialog.open(TrainingAddComponent, {
          panelClass: 'modal2',
          width: '1000px',
          //  height: '80%',
          data: dialogConfig.data,
          autoFocus: true,
        });
        message = 'Training added';
        break;
      }
      case 'request': {
        dialogConfig.data = {
          id: training.id,
          title: 'Training request',
          training : training,
          training_id: training.id,
        };
        const scrollStrategy = this.overlay.scrollStrategies.reposition();
        dialogRef = this.dialog.open(TrainingRequestComponent, {
          panelClass: 'myapp-no-padding-dialog',
          width: '1000px',
          //  height: '80%',
          data: dialogConfig.data,
          autoFocus: true,
          // scrollStrategy,
        });
        message = 'Request sent!';
        break;
      }
      default: {
        dialogConfig.data = {
          id: 1,
          title: 'Training request',
          training : training,
        };
        dialogRef = this.dialog.open(TrainingRequestComponent, dialogConfig);
        message = 'Request sent';
        break;
      }
    }


    dialogRef.afterClosed().subscribe(result => {
      if (result === 'saved') {
        this.toastr.success(message, '' , {
          positionClass: 'toast-bottom-center',
          timeOut: 3000
        });
      }
    }
    );
  }

}


// 2nd class to export: for Angular material table, this class will be used
// in the class "TrainingDataSource" defined below, therefore they should
// stay together
export class TrainingDatabase {

  trainingList = new BehaviorSubject([]);
  get data() {
    return this.trainingList.value;
  }

  constructor(private tfs: TrainingService) {
    this.tfs.getTrainingsFilter().subscribe((data) => {
      this.trainingList.next(data);
    });
  }
}


// 3rd class to export: for sorting; see the videos saved in Michal's Angular documentation
// for filtering
export class TrainingDataSource extends DataSource<any> {

  constructor(
    private trainingDB: TrainingDatabase,
  ) {
    super();
  }

  connect(): Observable<any> {
    const trainingData = [
    this.trainingDB.trainingList,
    // this.sort.sortChange
    ];

    return Observable.merge(...trainingData).map(() => {
      // return this.getSortedData();
      return this.trainingDB.data.slice();
    });
  }

  disconnect() {
  }

}

// 4th class to export: for filtering
export class FilteredDataSource extends DataSource<any> {

  constructor(private inputobs) {
    super();
  }

  connect(): Observable<any> {
    return this.inputobs;
  }

  disconnect() {
  }
}
