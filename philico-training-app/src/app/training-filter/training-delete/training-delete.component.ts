import { Component, OnInit, Inject } from '@angular/core';
import {TrainingService} from '../../core/training.service';
import {RequestService} from '../../core/request.service';
import {MAT_DIALOG_DATA, MatDialogRef, MatFormFieldModule } from '@angular/material';
import { FormBuilder, FormGroup } from '@angular/forms';
import {Training} from '../../core/training.model';
import { Trainee } from '../../core/trainee.model';
import { Request } from '../../core/request.model';


@Component({
  selector: 'app-training-delete',
  templateUrl: './training-delete.component.html',
  styleUrls: ['./training-delete.component.scss']
})
export class TrainingDeleteComponent implements OnInit {

	form: FormGroup;
  description: string;
  training_id: string;
  title: string;
  training: Training = {
    Name: '',
    Location: '',
    Costs: 0,
    Language: '',
    Category: '',
    Duration: 0,
    Institute: '',
    Certification: '',
    Description: '',
    Link: null,
    id: '',
    relatedContent: '',
  };
  trainees: Trainee[];

  requestDetails: Request = {
    status: 'Pending',
    };

  CategoryHasError = true;
  Target_groupHasError = true;
  LimitErrorCost = false;
  LanguageHasError = false;
  LimitErrorDuration = false;

  constructor(public trainingService: TrainingService,
    public requestService: RequestService,
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<TrainingDeleteComponent>,
    @Inject(MAT_DIALOG_DATA) data) {
    this.description = data.description;
    this.title = data.title;
    this.training = data.training;
    this.training_id = data.id; }

  ngOnInit() {
  	this.form = this.fb.group({
      description: [this.description, []],
    });
  }


  deleteTraining(training) {
    if ( confirm('Are you sure to delete ' + training.Name + '?')) {
      this.trainingService.deleteTraining(training, training.id);
    }
  }




  close() {
    this.dialogRef.close('Close');
  }

  save() {
    this.training.lastUpdatedBy = sessionStorage.getItem('email');
    // this.trainingService.updateTraining(this.training, this.training_id);
    this.trainingService.deleteTraining(this.training, this.training_id);
    this.dialogRef.close('saved');
  }


}
