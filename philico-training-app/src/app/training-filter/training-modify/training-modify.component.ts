import { Component, OnInit, Inject, Input } from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef, MatFormFieldModule } from '@angular/material';
import { FormBuilder, FormGroup } from '@angular/forms';
import {Training} from '../../core/training.model';
import {TrainingService} from '../../core/training.service';
import {RequestService} from '../../core/request.service';
import { Trainee } from '../../core/trainee.model';
import { Request } from '../../core/request.model';

@Component({
  selector: 'app-training-modify',
  templateUrl: './training-modify.component.html',
  styleUrls: ['./training-modify.component.scss']
})
export class TrainingModifyComponent implements OnInit {
  form: FormGroup;
  description: string;
  training_id: string;
  title: string;
  training: Training = {
    Name: '',
    Location: '',
    Costs: 0,
    Language: '',
    Category: '',
    Duration: 0,
    Institute: '',
    Certification: '',
    Description: '',
    Link: null,
    id: '',
    relatedContent: '',
  };
  trainees: Trainee[];

  requestDetails: Request = {
    status: 'Pending',
    };

  CategoryHasError = true;
  Target_groupHasError = true;
  LimitErrorCost = false;
  LanguageHasError = false;
  LimitErrorDuration = false;


  userRoleModfiy: string;

  
  constructor(
    public trainingService: TrainingService,
    public requestService: RequestService,
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<TrainingModifyComponent>,
    @Inject(MAT_DIALOG_DATA) data) {

    this.description = data.description;
    this.title = data.title;
    this.training = data.training;
    this.training_id = data.id;
    this.userRoleModfiy = data.userRole;


}

  ngOnInit() {
    this.form = this.fb.group({
      description: [this.description, []],
    });

  }

  checkLimit(number, type){
    if(type == 'Cost'){
      if(number < 0){
        this.LimitErrorCost = true;
        return true
      }if(number > 7500){
        this.LimitErrorCost = true;
        return true
      }else{
        this.LimitErrorCost = false;
        return false
      }

    }if(type == 'Duration'){
      if(number < 0){
        this.LimitErrorDuration = true;
        return true
      }if(number > 15){
        this.LimitErrorDuration = true;
        return true
      }else{
        this.LimitErrorDuration = false;
        return false
      }
    }
  }

  validateCategory(value) {
      this.CategoryHasError = (value === 'default');
  }

  validateLanguage(value) {
      this.LanguageHasError = (value === 'default');
  }

  validateTarget_group(value) {
      this.Target_groupHasError = (value === 'default');
  }


  close() {
    this.dialogRef.close('Close');
  }

  save() {
    this.training.lastUpdatedBy = sessionStorage.getItem('email');
    this.trainingService.updateTraining(this.training, this.training_id);
    this.dialogRef.close('saved');
  }


}
