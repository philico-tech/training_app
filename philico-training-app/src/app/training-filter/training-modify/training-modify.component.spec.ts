import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrainingModifyComponent } from './training-modify.component';

describe('TrainingModifyComponent', () => {
  let component: TrainingModifyComponent;
  let fixture: ComponentFixture<TrainingModifyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrainingModifyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrainingModifyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
