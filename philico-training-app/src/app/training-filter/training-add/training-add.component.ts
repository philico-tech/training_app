import { Component, OnInit } from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef, MatFormFieldModule } from '@angular/material';
import { FormBuilder, FormGroup } from '@angular/forms';
import {Training} from '../../core/training.model';
import {TrainingService} from '../../core/training.service';
import {RequestService} from '../../core/request.service';
import { Trainee } from '../../core/trainee.model';
import { Request } from '../../core/request.model';
import { HttpClient } from '@angular/common/http';
import { UserService } from '../../core/user.service';
import { environment } from '../../../environments/environment'; // for the endpoint

@Component({
  selector: 'app-training-add',
  templateUrl: './training-add.component.html',
  styleUrls: ['./training-add.component.scss']
})
export class TrainingAddComponent implements OnInit {

  form: FormGroup;
  description: string;
  training_id: string;
  title: string;
  training: Training = {
    Name: '',
    Location: '',
    Costs: null,
    Category: 'default',
    Duration: null,
    Target_group: 'default',
    Language: 'default',
    Institute: '',
    Certification: '',
    Description: '',
    Link: null,
    trainingID: '',
    insertedBy: '',
    relatedContent: '',
    active: '',
  };
  trainees: Trainee[];
  current_user: Trainee;

  requestDetails: Request = {
    status: 'Pending',
    };

  CategoryHasError = false;
  LanguageHasError = false;
  Target_groupHasError = false;
  LimitErrorCost = false;
  LimitErrorDuration = false;

  constructor(
    private http: HttpClient,
    public userService: UserService,
    public trainingService: TrainingService,
    public requestService: RequestService,
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<TrainingAddComponent>,
    ) {

  }

  ngOnInit() {
    this.form = this.fb.group({
      description: [this.description, []],
    });

    this.userService.get_current_user().subscribe(
      current_user => {
        this.current_user = current_user;
      }
    );

  }

  checkLimit(number, type){
    if(type == 'Cost'){
      if(number < 0){
        this.LimitErrorCost = true;
        return true
      }if(number > 7500){
        this.LimitErrorCost = true;
        return true
      }else{
        this.LimitErrorCost = false;
        return false
      }

    }if(type == 'Duration'){
      if(number < 0){
        this.LimitErrorDuration = true;
        return true
      }if(number > 15){
        this.LimitErrorDuration = true;
        return true
      }else{
        this.LimitErrorDuration = false;
        return false
      }
    }
  }

  validateCategory(value) {
    // if (value === 'default') {
      this.CategoryHasError = (value === 'default');
    // } else {
      // this.CategoryHasError = false;
    // }
  }

  validateTarget_group(value) {
    // if (value === 'default') {
      this.Target_groupHasError = (value === 'default');
    // } else {
      // this.Target_groupHasError = false;
    // }
  }

  validateLanguage(value) {
    // if (value === 'default') {
      this.LanguageHasError = (value === 'default');
    // } else {
      // this.Target_groupHasError = false;
    // }
  }

  close() {
      this.dialogRef.close('Close');
  }

  save() {
    this.training.active = "true";

    if (this.training.Name !== '') {
      this.training.insertedBy = sessionStorage.getItem('email');
      this.training.lastUpdatedBy = sessionStorage.getItem('email');
      this.training.trainingID = this.training.Name.split(' ', 1) + this.training.Location + String(Date.now());
      this.trainingService.addTrainings(this.training);
    }

    const endpoint = environment.fbCloudFunHttpEmailEndpoint;

    const data = {
      toEmail: sessionStorage.getItem('email'),
      toName: this.current_user.Coach,
      toFirstName: this.current_user.CoachFirstName,
      traineeCoachFirstName : this.current_user.CoachFirstName,
      fromName: this.current_user.Name,
      fromFirstName: this.current_user.FirstName,
      trainingName: this.training.Name,
      fromEmail: sessionStorage.getItem('email'),
      subject:'Training App Notification: ' +  this.current_user.FirstName + ' added the training ' + this.training.Name,
      message: this.current_user.FirstName + ' added the training ' + this.training.Name,
    };
    this.http.post(endpoint, data).subscribe();

    this.dialogRef.close('saved');
  }

}
