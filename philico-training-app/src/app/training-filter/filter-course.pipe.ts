import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterCourse'
})
export class FilterCoursePipe implements PipeTransform {

  transform(request: any, term: any): any {
   //Check if Search is undefined
   if(term === undefined) return request;
   //return updated request array
   return request.filter(function(request){
   	return request.Name.toLowerCase().includes(term.toLowerCase()) 
   	    || request.Language.toLowerCase().includes(term.toLowerCase())
   	    || request.Location.toLowerCase().includes(term.toLowerCase())
   	    || request.Category.toLowerCase().includes(term.toLowerCase())
        || request.Target_group.toLowerCase().includes(term.toLowerCase())
        || request.Description.toLowerCase().includes(term.toLowerCase())
        || request.Costs.toString().includes(term)
        || request.Duration.toString().includes(term)
  	    ; // False or true
   })
  }
  
}
