
import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatSort, MatDialog, MatDialogConfig, MatSnackBar, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { ActivatedRoute, Params } from '@angular/router';
import { FormBuilder, FormGroup, FormGroupDirective } from '@angular/forms';
import { UserService } from '../../core/user.service';
import { Training } from '../../core/training.model';
import { Request } from '../../core/request.model';
import { TrainingService } from '../../core/training.service';
import { RequestService } from '../../core/request.service';
import { RatingService } from '../../core/rating.service';
import { Trainee } from '../../core/trainee.model';
import { Rating } from '../../core/rating.model';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs/Observable';
import {MatExpansionModule} from '@angular/material/expansion';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment'; // for the endpoint
import {formatDate} from '@angular/common';

@Component({
  selector: 'app-training-request',
  templateUrl: './training-request.component.html',
  styleUrls: ['./training-request.component.scss']
})
export class TrainingRequestComponent implements OnInit {
  panelOpenState = false;
  trainingID: string;
  userID: string;
  userName: string;
  userFirstName: string;
  userBudgetMoney:number;
  userBudgetTime:number;
  userPosition:string;
 
  selectedSubWindow: string;

  current_user: Trainee;

  requestDetails: Request = {
    status: 'Pending',
    };

  form: FormGroup;
  description: string;
  title: string;
  training: Training = {
    Name: '',
    Location: '',
    Costs: 0,
    Category: '',
    Duration: 0,
    Institute: '',
    Certification: '',
    Description: '',
    Link: null,
    id: '',
    trainingCostProject:0,
    trainingDurationProject:0
  };
  trainees: Trainee[];
  trainings: Training = {
    Name: '',
    Location: '',
  };

  rating: Rating = {
    trainingID: '',
    userName: '',
    userID: '',
    value: null,
    comments: '',
    timestamp: null,
  };

  currentRating: Rating[];


  // stars: any;
  // avgRating: any;
  stars: Observable<any>;
  avgRating: Observable<any>;


  dateCheckS:boolean;
  dateCheckE:boolean;

  CheckEligibility:boolean;

  LimitErrorCost:boolean;
  LimitErrorDuration:boolean;

  LimitBudgetCost:boolean;
  LimitBudgetDuration:boolean;
  
  // error:any={isError:false,errorMessage:''};

  constructor(
    public userService: UserService,
    private ActiveRoute: ActivatedRoute,
    public trainingService: TrainingService,
    public requestService: RequestService,
    public ratingService: RatingService,
    private fb: FormBuilder,
    private http: HttpClient,
    public dialog: MatDialog,
    private toastr: ToastrService,
    public dialogRef: MatDialogRef<TrainingRequestComponent>,
    @Inject(MAT_DIALOG_DATA) data) {

      this.description = data.description;
      this.title = data.title;
      this.training = data.training;
      this.trainingID = data.training_id;
      dialogRef.disableClose = false;
    }

  ngOnInit() {

    this.form = this.fb.group({
      description: [this.description, []],
    });

    const aux = sessionStorage.getItem('email');
    this.ratingService.getRating(this.training.trainingID, aux).subscribe(
      rating => {
        if (rating.length !== 0) {
        this.currentRating = rating;
        }
      }
    )

    this.userService.get_current_user().subscribe(
      current_user => {
        this.current_user = current_user;
        this.userName = current_user.Name;
        this.userFirstName = current_user.FirstName;
        this.userBudgetTime = current_user.budgetLeftDays;
        this.userBudgetMoney = current_user.budgetLeftMoney;
        this.userPosition = current_user.Position;
      }
    );

    // this.positionCheck();

    }


  positionCheck(eleg){
    let positionToNum = 0;
    let eligToNum = 0;

   switch(this.userPosition) { 
     case "Junior": { 
        positionToNum = 1; 
        break; 
     } 
     case "Consultant": { 
        positionToNum = 2; 
        break; 
     } 
     case "Senior": {
        positionToNum = 3;
        break;    
     } 
     case "Manager": { 
        positionToNum = 4; 
        break; 
     }  
     default: { 
        positionToNum = 1; 
        break;              
     } 
   }

   switch(eleg) { 
     case "Junior": { 
        eligToNum = 1;
        break; 
     } 
     case "Consultant": { 
        eligToNum = 2;
        break; 
     } 
     case "Senior": {
        eligToNum = 3;
        break;    
     } 
     case "Manager": { 
        eligToNum = 4;
        break; 
     }  
     default: { 
        eligToNum = 4;
        break;              
     } 
   }


    if(positionToNum >=eligToNum){ 
      // console.log("Ok");
      this.CheckEligibility = false;
      return true
    }else{
      // console.log("NOT ok");
      this.CheckEligibility = true;
      return false
    }
  }


  compareStartDate(start, end){
    if(end < start){
      // console.log('Start:---------' +'check function- Start: '+ start + '  End: ' + end + '  ');
      this.dateCheckS = true;
      return 0;
    }else{
      // console.log('Start:+++++++++' +'check function- Start: '+ start + '  End: ' + end + '  ');
      this.dateCheckS = false;
      return 0;
    }
  }


  compareEndDate(start, end){
    if(this.requestDetails.dateEnd < start){
      // console.log('End:---------'+'check function- End: '+ start + '  End: ' + end + '  ');
      this.dateCheckE = true;
      return 0;
    }else{
      // console.log('End:+++++++++'+'check function- End: '+ start + '  End: ' + end + '  ');
      this.dateCheckE = false;
      return 0;
    }
  }


  ngAfterViewInit() {
    this.userService.get_current_user().subscribe(
      current_user => {
        this.current_user = current_user;
        // console.log('current user name: ' + current_user.Name);
        this.userName = current_user.Name;
        this.userFirstName = current_user.FirstName;
        this.userPosition = current_user.Position;
      }
    );

    // console.log('userName: ' + this.userName);
    this.stars = this.ratingService.getTrainingStars(this.trainingID);
    //console.log('trainingID: ' + this.trainingID);
    //console.log('stars: ' + JSON.stringify(this.stars));
    // this.ratingTimestamp = this.stars.timestamp.toLocaleDateString();


    this.avgRating = this.stars.map(arr => {
      const ratings = arr.map(v => v.value);
      return ratings.length ? ratings.reduce((total, val) => total + val) / arr.length : 'not reviewed';
    });

  }

  starHandler(value) {
    this.rating.value = value;
    // console.log('rating: ' + this.rating.value);
    // console.log('comments: ' + this.rating.comments);
    // this.userID = this.userService.getUserID();
    // this.ratingService.setStar(this.userID, this.trainingID, this.rating.value, this.rating.comments);
  }

  starSubmit() {
    const ratingName = this.userFirstName + ' ' + this.userName;
    this.userID = this.userService.getUserID();
    if (this.currentRating === undefined) {
      this.ratingService.setStar(this.userID, ratingName, this.trainingID, this.rating.value, this.rating.comments);
    } else {
      this.currentRating[0].value= this.rating.value;
      this.currentRating[0].timestamp = new Date();
      this.ratingService.updateStar(this.currentRating[0], this.trainingID, this.userID);

    }
    this.avgRating.subscribe(
      rating => {
        this.trainingService.setAvgRating(rating, this.trainingID);
      }
    )
    //console.log('submit test');
    this.dialogRef.close('Close');
  }

  checkCurrentRating() {

    if (this.currentRating !== undefined) {
      return true;
    } else {
      return false;
    }
  }
  
   checkLimit(number, type){
    if(type == 'Cost'){
      if(number < 0){
        this.LimitErrorCost = true;
        return true
      }if(number > 7500){
        this.LimitErrorCost = true;
        return true
      }else{
        this.LimitErrorCost = false;
        return false
      }

    }if(type == 'Duration'){
      if(number < 0){
        this.LimitErrorDuration = true;
        return true
      }if(number > 15){
        this.LimitErrorDuration = true;
        return true
      }else{
        this.LimitErrorDuration = false;
        return false
      }
    }
  }


 checkBudget(number, type){
 // console.log(this.userBudgetTime + " and  " + this.userBudgetMoney);
    if(type == 'Cost'){

      if(number > this.userBudgetMoney){
        this.LimitBudgetCost = true;
        return true
      }else{
        this.LimitBudgetCost = false;
        return false
      }

    }if(type == 'Duration'){
      if(number > this.userBudgetTime){
        this.LimitBudgetDuration = true;
        return true
      }else{
        this.LimitBudgetDuration = false;
        return false
      }
    }
  }



  close() {
    this.dialogRef.close('Close');
  }

  // the following property calls the cloud function specified using "this.endpoint"
  sendEmail() {
    const endpoint = environment.fbCloudFunHttpEmailEndpoint;
    // console.log(endpoint);
    // console.log("training-request");



    const data = {
      toEmail: this.current_user.Coach,
      toFirstName: this.current_user.CoachFirstName,
      fromEmail: this.current_user.email,
      subject: 'Training App Notification: ' + this.userFirstName + ' submitted a new request for the training ' + this.training.Name,
      message: 'You have a new pending request from ' + this.userFirstName + ' for the training "' + this.training.Name + '".',
    };

    // console.log('toEmail: ' + data.toEmail );
    // console.log('toFirstName: ' + data.toFirstName );
    // console.log('fromEmail: ' + data.fromEmail );
    // console.log('subject: ' + data.subject );
    // console.log('message: ' + data.message );




    
    this.http.post(endpoint, data).subscribe();

    // this.requestDetails.dateEnd = this.training.Name;
    this.requestDetails.trainingName = this.training.Name;
    this.requestDetails.location = this.training.Location;
    this.requestDetails.traineeCoach = this.current_user.Coach;
    this.requestDetails.traineeCoachFirstName = this.current_user.CoachFirstName;
    this.requestDetails.traineeName = this.current_user.Name;
    this.requestDetails.traineeFirstName = this.userFirstName;
    this.requestDetails.trainingDuration = this.training.Duration;
    this.requestDetails.trainingCost = this.training.Costs;
    this.requestDetails.traineeEmail = this.current_user.email;
    this.requestDetails.dateBudget = formatDate(this.requestDetails.dateStart, 'yyyy', 'en');
    this.requestDetails.dateRequest = new Date();
    this.requestDetails.courseLink = this.training.Link;
    this.requestDetails.instituteName = this.training.Institute;
    this.requestDetails.certifTitle = this.training.Certification;
   if(Number(formatDate(this.current_user.startContractDate, 'yyyy', 'en'))+1 >= Number(formatDate(this.requestDetails.dateRequest, 'yyyy', 'en')) && Number(formatDate(this.current_user.startContractDate, 'mm', 'en')) >= Number(formatDate(this.requestDetails.dateRequest, 'mm', 'en'))){
   this.requestDetails.rembourse = "To be rembourse";
   } else{
    this.requestDetails.rembourse = "To not be rembourse";
   }
    



    // this.requestDetails.id = this.training.id;



    this.requestService.addRequest(this.requestDetails);
    // console.log(ErrorEvent);
    this.dialogRef.close('saved');
  }

  showDescription() {
    this.selectedSubWindow = 'description';
    document.getElementById("training_description").style.display = "";
    document.getElementById("rate_training").style.display = "none";
    document.getElementById("request_training").style.display = "none";
    }
    
  showRating() {
    this.selectedSubWindow = 'rating';
    document.getElementById("rate_training").style.display = "";
    document.getElementById("training_description").style.display = "none";
    document.getElementById("request_training").style.display = "none";
    }  

  showRequest() {
    this.selectedSubWindow = 'request';
    document.getElementById("request_training").style.display = "";
    document.getElementById("rate_training").style.display = "none";
    document.getElementById("training_description").style.display = "none";
    }  


     
}

