import { ActivatedRoute } from '@angular/router';
import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { Training } from './training.model';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';


@Injectable()
export class TrainingService {
  trainingsCollection: AngularFirestoreCollection<Training>;
  trainings: Observable<Training[]>;
  // specificTraining: Observable<any>;
  trainingsDoc: AngularFirestoreDocument<Training>;

  private specificTrainingDoc: AngularFirestoreDocument<Training>;
  specificTraining: Observable<Training>;

  constructor(
   public db: AngularFirestore,
   public afAuth: AngularFireAuth,
   private route: ActivatedRoute ) {
    // this.specificTraining = this.db.collection('Training').get();

    this.trainings = this.db.collection('Training').snapshotChanges().pipe(
      map(changes => {
        return changes.map(a => {
          const data = a.payload.doc.data() as Training;
          data.id = a.payload.doc.id;
          return data;
        });
      })
    );
    this.trainingsCollection = this.db.collection('Training', ref => ref.orderBy('Location'));
  }

 getTrainings() {
    return this.trainings;
 }

 addTrainings(trainings: Training) {
    this.trainingsCollection.doc(trainings.trainingID).set(trainings);
 }

deleteTraining(training, trainingId) {
  this.trainingsDoc = this.db.doc(`Training/${trainingId}`);
  this.trainingsDoc.delete();
}

updateTraining(training, trainingId) {
  this.trainingsDoc = this.db.doc(`Training/${trainingId}`);
  this.trainingsDoc.update(training);
}

getSpecificTraining(trainingId) {
  this.specificTrainingDoc = this.db.doc<Training>(`Training/${trainingId}`);
  this.specificTraining = this.specificTrainingDoc.valueChanges();
  return this.specificTraining;

}


getTrainingsFilter() {
  // return this.afs.collection('Training', ref => ref.orderBy('Name', 'asc')).valueChanges();
  this.trainings = this.db.collection('Training').snapshotChanges().pipe(
    map(changes => {
      return changes.map(a => {
        const data = a.payload.doc.data() as Training;
        data.id = a.payload.doc.id;
        return data;
      });
    })
  );
  return this.trainings;
  // this.trainingsCollection = this.afs.collection('Training', ref => ref.orderBy('Location'));
}

  filterData(customfilters) {
    return new Promise((resolve, reject) => {

      if (customfilters.Target_group === null) {
        reject();
      }

      resolve(this.db.collection('Training', ref =>
        ref.where( 'Target_group', '==', customfilters.Target_group)).valueChanges());
    });
  }

  getTrainingID() {
    return this.trainingsDoc.ref.id;
  }

  setAvgRating(rating, trainingID) {
    this.db.collection('Training').doc(trainingID).update({'avgRating' : rating});
  }

}


