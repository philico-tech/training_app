export interface Trainee {
  id?: any;
  FirstName?: string;
  Name?: string;
  Position?: string;
  Coach?: string;
  CoachFirstName?: string;
  BudgetMoney?: number;
  BudgetDays?: number;
  email?: string;
  userRole?: string;
  startContractDate?: string;
}
