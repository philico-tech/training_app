
import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import * as firebase from 'firebase/app';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';


@Injectable()
export class AuthService {

  private mail = new BehaviorSubject<string>('default');
  loggedInUserEmail = this.mail.asObservable();
  email: string;

  constructor(
    public afAuth: AngularFireAuth
  ) {
    this.loggedInUserEmail.subscribe(mail => this.email = mail);


  }

  doGoogleLogin() {
    return new Promise<any>((resolve, reject) => {
      const provider = new firebase.auth.GoogleAuthProvider();
      provider.addScope('profile');
      provider.addScope('email');
      this.afAuth.auth
      .signInWithPopup(provider)
      .then( res => {
        resolve(res);
        this.loggedInUserEmail.subscribe(mail => this.email = mail);
        // console.log(this.email);
        this.mail.next(firebase.auth().currentUser.email);
        this.loggedInUserEmail.subscribe(mail => this.email = mail);
        sessionStorage.setItem('email', this.email);
      }, err => {
        // console.log(err);
        reject(err);
      });
    });
  }


  doLogout() {
    return new Promise((resolve, reject) => {
      if (firebase.auth().currentUser) {
        this.afAuth.auth.signOut();
        resolve();
      } else {
        reject();
      }
    });
  }




}
