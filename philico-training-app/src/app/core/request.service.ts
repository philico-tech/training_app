import { Injectable } from '@angular/core';
import { Trainee } from './trainee.model';
import { Request } from './request.model';
import { ActivatedRoute } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { Observable } from 'rxjs';
import { map,  switchMap } from 'rxjs/operators';
import { FirebaseUserModel } from './user.model';


// @Injectable({
//   providedIn: 'root'
// })

export interface Star {
  userId: any;
  movieId: any;
  value: number;
}

@Injectable()
export class RequestService {
  requestsCollection: AngularFirestoreCollection<Request>;
  user_doc_id: Trainee;
  current_user: Observable<any>;
  requestModifDoc: AngularFirestoreDocument<Request>;

  user: FirebaseUserModel = new FirebaseUserModel();
  requests: Observable<Request[]>;
  requestsDoc: AngularFirestoreDocument<Request>;
  current_request: any;



  constructor(
    private afs: AngularFirestore
  ) {
      this.requestsCollection = this.afs.collection('Requests');
  }


updateRequest(request, requestId) {
  this.requestModifDoc = this.afs.doc(`Requests/${requestId}`);
  this.requestModifDoc.update(request);
}

  addRequest(requestData) {
    this.requestsCollection.add(requestData);
  }


  getRequests() {
    return this.afs.collection('Requests').snapshotChanges().pipe(
      map(changes => {
        return changes.map(a => {
          const data = a.payload.doc.data() as Request;
          data.id = a.payload.doc.id;
          return data;
        });
      })
    );
  }



  getRequestsTrainee(emailTrainee) {
    return this.afs.collection('Requests', ref => ref.where(
   'traineeEmail', '==', emailTrainee)).snapshotChanges().pipe(
      map(changes => {
        return changes.map(a => {
          const data = a.payload.doc.data() as Request;
          data.id = a.payload.doc.id;
          return data;
        });
      })
    );
  }


  getRequestsCoach() {
    const emailCoach = sessionStorage.getItem('email');
    return this.afs.collection('Requests', ref => ref.where(
   'traineeCoach', '==', emailCoach)).snapshotChanges().pipe(
      map(changes => {
        return changes.map(a => {
          const data = a.payload.doc.data() as Request;
          data.id = a.payload.doc.id;
          return data;
        });
      })
    );
  }



}
