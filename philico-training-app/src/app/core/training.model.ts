export interface Training {
  id?: any;
  Name?: string;
  Location?: string;
  Description?: string;
  Costs?: number;
  Category?: string;
  Duration?: number;
  Target_group?: string;
  Institute?: string;
  Certification?: string;
  Link?: URL;
  trainingID?: string;
  Language?: string;
  insertedBy?: string;
  lastUpdatedBy?: string;
  relatedContent?: string;
  avgRating?: number;
  trainingCostProject?:number;
  trainingDurationProject?:number;
  active?:string;
}
