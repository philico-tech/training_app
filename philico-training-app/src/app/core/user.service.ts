import { ActivatedRoute } from '@angular/router';
import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import * as firebase from 'firebase/app';
import { Training } from './training.model';
import { Trainee } from './trainee.model';
import { Request } from './request.model';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';
import { map,  switchMap } from 'rxjs/operators';
import { FirebaseUserModel } from './user.model';
import {formatDate} from '@angular/common';


@Injectable()
export class UserService {

  trainees: Observable<Trainee[]>;
  current_trainee: Observable<any>;
  user: FirebaseUserModel = new FirebaseUserModel();
  email: string;
  userDoc: AngularFirestoreDocument<any>;

  current_request:any;
  current_user;
  userRequestArray;
  currentUser: Observable<any>;
  current_userNA;


  user_doc_id: Trainee;

  constructor(
   public afs: AngularFirestore,
   private ds: AuthService,
   public afAuth: AngularFireAuth,
   private route: ActivatedRoute
 ) {

  // const aaa = sessionStorage.getItem('email');
  // this.afs.collection('Users').doc(aaa).valueChanges().subscribe( currentUser => {
  //   this.current_user = currentUser;
  // })

  this.route.data.subscribe(routeData => {
    const data = routeData['data'];
    if (data) {
      this.user = data;
    }
  });

  const aux = sessionStorage.getItem('email');
    this.trainees = this.afs.collection('Users').doc(aux).collection('Trainings_taken').snapshotChanges().pipe(
    map(changes => {
      return changes.map(a => {
        const data = a.payload.doc.data() as Trainee;
        data.id = a.payload.doc.id;
        return data;
      });
    })
  );
 }

 getTraineeApprovedRequest(){
    const aux = sessionStorage.getItem('email');
    return this.afs.collection('Requests', ref => ref.where(
   'traineeEmail', '==', aux)).snapshotChanges().pipe(  //.where('status', '==', 'Approved')
      map(changes => {
        return changes.map(a => {
          const data = a.payload.doc.data() as Request;
          data.id = a.payload.doc.id;
          return data;
        });
      })
      );
  }


   // get the user's data
 get_fs_user() {
  const aux = sessionStorage.getItem('email');
  return this.afs.collection('Users', ref => ref.where(
      'email', '==',  aux )).snapshotChanges().pipe(
      map(changes => {
        return changes.map(a => {
          const data = a.payload.doc.data() as Trainee;
          data.id = a.payload.doc.id;
          return data;
        });
      })
      );
  }



  getTrainees() {
    // return this.trainees;
    const aux = sessionStorage.getItem('email');
    this.trainees = this.afs.collection('Users').doc(aux).collection('Trainings_taken').snapshotChanges().pipe(
      map(changes => {
        return changes.map(a => {
          const data = a.payload.doc.data() as Trainee;
          data.id = a.payload.doc.id;
          return data;
        });
      })
    );
  }


getCurrentUser() {
    return new Promise<any>((resolve, reject) => {
      const currentUser = firebase.auth().onAuthStateChanged(function(user) {
        if (currentUser) {
          resolve(user);
        } else {
          console.log('User service currentUser NOT logged in: ' + currentUser);
          reject('No user logged in');
        }
      });
    });
  }



// get the user's data
  get_current_user() {
    const aux = sessionStorage.getItem('email');
    // console.log('storage aux: ' + aux);
    this.current_trainee = this.afs.collection('Users').doc(aux).valueChanges();
    return this.current_trainee;
  }



getUserID() {
  return sessionStorage.getItem('email');
}

updateBudget(user, userRequestArray) {
  // console.log('debut function');
  // console.log('updateBudget user: ' + user.Name);
  const currentYear = new Date().getFullYear();
  let currentMoneyYear = 0; //Used for chart %
  let currentDayYear = 0;   //Used for chart %
  let currentBudgetCalculation =[];
  let percentageMoneyCY = 0;  //Used for chart %
  let percentageTimeCY = 0;   //Used for chart %

  //T: Time     M: Money
  let totalMY = 0;
  let totalTY = 0;
  let startYear = 0;
  let startMonth = 0;
  let memoryMoney = 0;
  let memoryTime = 0;
  let totalMoneyP = 0;
  let totalTimeP = 0;
  let moneyBudget = 5000; //to use dynamic variable from collection Budget_Parameter
  let timeBudget = 10;    //to use dynamic variable from collection Budget_Parameter
  let moneyBudgetLimit = 7500; //limit money
  let timeBudgetLimit = 15;    //limit time

  if(user.startContractDate == undefined){
    //In case starting date is undefined, it will set to the 06.2016 for testing
    startYear = 2016;
    startMonth = 6;
  }else{
    // console.log(Number(formatDate(user.startContractDate, 'yyyy', 'en')));
    // console.log(Number(user.startContractDate.slice(5,7)));
    startMonth = Number(user.startContractDate.slice(5,7));
    startYear = Number(formatDate(user.startContractDate, 'yyyy', 'en'));
    // startMonth = 6;
    // startYear = 2016;
  };

  for (var k = 2013; k < (Number(currentYear)+2); k++) {
    // console.log('year: ' + k);
    totalMY = 0;
    totalTY = 0;

    let numberCourse = 0;
    let pendingCost = 0;
    let pendingDay = 0;
    let budgetM = 0;
    let budgetT = 0;
    let minusMoney = 0; //It will be usefull in case Philico want to bill some extra cost
    let minusTime = 0;  //It will be usefull in case Philico want to bill some extra time cost

    let newElement = {
      year: 0,
      moneyBudget: 0,
      timeBudget: 0,
      totalMYear: 0,
      totalTYear: 0,
      AMB:0,
      ATB:0,
      numberC:0,
      pendingMoney:0,
      pendingTime:0,
      negativeMoney:0,
      negativeTime:0,
    }

    for (var i = 0; i < userRequestArray.length; i++) {
        if (userRequestArray[i].dateBudget == k && (userRequestArray[i].status == 'Approved'|| userRequestArray[i].status == 'Canceled')) {
            totalMY += Number(userRequestArray[i].trainingCostProject); //trainingCost
            totalTY += Number(userRequestArray[i].trainingDurationProject); //trainingDuration
            numberCourse += 1;
        }if (userRequestArray[i].dateBudget == k && userRequestArray[i].status == 'Pending') {
          pendingCost += Number(userRequestArray[i].trainingCostProject);
          pendingDay += Number(userRequestArray[i].trainingDurationProject);
        }
    };

    if(k < startYear){

      newElement = {
        year: k,
        moneyBudget: 0,
        timeBudget: 0,
        AMB: 0,
        ATB: 0,
        numberC: 0,
        pendingMoney: 0,
        pendingTime: 0,
        totalMYear: 0,
        totalTYear: 0,
        negativeMoney:0,
        negativeTime:0,
      };

    }if (k == startYear) {
      let MB = (moneyBudget*(13-startMonth)/12);
      let TB = (timeBudget*(13-startMonth)/12);

      // // Check remaining Budgets Pendind NOT influence remaining budget
      // if( 0 > (MB-totalMY)){
      //   budgetM = 0;
      //   minusMoney = MB-totalMY;
      // }else{
      //   budgetM = MB-totalMY;
      //   minusMoney = 0;
      // };

      // if(0 > (TB-totalTY)){
      //   budgetT = 0;
      //   minusTime = TB-totalTY;
      // }else{
      //   budgetT = TB-totalTY;
      //   minusTime = 0;
      // };


      // Check remaining Budgets Pendind influence remaining budget
      if( 0 > (MB-totalMY)){
        budgetM = 0;
        minusMoney = MB-totalMY;
      }else{
        budgetM = MB-totalMY - pendingCost;
        minusMoney = 0;
      };

      if(0 > (TB-totalTY)){
        budgetT = 0;
        minusTime = TB-totalTY;
      }else{
        budgetT = TB-totalTY - pendingDay;
        minusTime = 0;
      };


      memoryMoney = budgetM;
      memoryTime = budgetT;
      totalMoneyP = MB;
      totalTimeP = TB;

      newElement = {
        year: k,
        moneyBudget: budgetM,
        timeBudget: budgetT,
        AMB: MB,
        ATB: TB,
        numberC: numberCourse,
        pendingMoney: pendingCost,
        pendingTime: pendingDay,
        totalMYear: totalMY,
        totalTYear: totalTY,
        negativeMoney:minusMoney,
        negativeTime:minusTime,
      };

    }if (k > startYear){
      let MB = 0;
      let TB = 0;
      // Check limits Budgets
      if( moneyBudgetLimit > (moneyBudget + memoryMoney)){
        MB = moneyBudget + memoryMoney;
      }else{
        MB = moneyBudgetLimit;
      };

      if( timeBudgetLimit > (timeBudget + memoryTime)){
        TB = timeBudget + memoryTime;
      }else{
        TB = timeBudgetLimit;
      };

      totalMoneyP = MB;
      totalTimeP = TB;

      // // Check remaining Budgets
      // if( 0 > (MB-totalMY)){
      //   budgetM = 0;
      //   minusMoney = MB-totalMY;  //It will be usefull in case Philico want to bill some extra costs
      // }else{
      //   budgetM = MB-totalMY;
      //   minusMoney = 0;
      // };

      // if(0 > (TB-totalTY)){
      //   budgetT = 0;
      //   minusTime = TB-totalTY;  //It will be usefull in case Philico want to "bill" some extra time
      // }else{
      //   budgetT = TB-totalTY;
      //   minusTime = 0;
      // };


     // Check remaining Budgets Pendind influence remaining budget
      if( 0 > (MB-totalMY)){
        budgetM = 0;
        minusMoney = MB-totalMY;
      }else{
        budgetM = MB-totalMY - pendingCost;
        minusMoney = 0;
      };

      if(0 > (TB-totalTY)){
        budgetT = 0;
        minusTime = TB-totalTY;
      }else{
        budgetT = TB-totalTY - pendingDay;
        minusTime = 0;
      };

      memoryMoney = budgetM;
      memoryTime = budgetT;

      newElement = {
        year: k,
        moneyBudget: budgetM,
        timeBudget: budgetT,
        AMB: MB,
        ATB: TB,
        numberC: numberCourse,
        pendingMoney: pendingCost,
        pendingTime: pendingDay,
        totalMYear: totalMY,
        totalTYear: totalTY,
        negativeMoney:minusMoney,
        negativeTime:minusTime,
      };

    };
    if(k == Number(currentYear)){
      currentMoneyYear = budgetM
      currentDayYear = budgetT;
      percentageMoneyCY = 100*budgetM/totalMoneyP;
      percentageTimeCY = 100*budgetT/totalTimeP;
    };

    currentBudgetCalculation.push(newElement);

  }
      this.afs.collection('Users').doc(user.email).update({'budgetLeftMoney' : currentMoneyYear, 'budgetLeftDays' : currentDayYear});
      return {currentArray: currentBudgetCalculation, currentYearMoney: percentageMoneyCY, currentYearDay: percentageTimeCY};

}

}
