import { Injectable } from '@angular/core';
import { Trainee } from './trainee.model';
import {Rating} from './rating.model';
import { Request } from './request.model';
import { ActivatedRoute } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { Observable } from 'rxjs';
import { map,  switchMap } from 'rxjs/operators';
import { FirebaseUserModel } from './user.model';


export interface Star {
  userID: any;
  userName: string;
  trainingID: any;
  value: number;
  comments: string;
  timestamp: Date;
}

@Injectable({
  providedIn: 'root'
})
export class RatingService {
  // ratingCollection: AngularFirestoreCollection<Rating>;
  user_doc_id: Trainee;
  current_user: Observable<any>;
  ratingDoc: AngularFirestoreDocument<Rating>;

  constructor(
    private afs: AngularFirestore
  ) {
      // this.ratingCollection = this.afs.collection('Rating');
  }



// Star reviews that belong to a user
getUserStars(userId) {
  const starsRef = this.afs.collection('Rating', ref => ref.where('userID', '==', userId) );
  return starsRef.valueChanges();
}

// Get all stars that belog to a Movie
getTrainingStars(trainingID) {
  const starsRef = this.afs.collection('Rating', ref => ref.where('trainingID', '==', trainingID) );
  return starsRef.valueChanges();
}

getRating(trainingID, user) {
  // const rating = this.afs.collection('Rating', ref => ref.where('trainingID', '==', trainingID).where('userID', '==', user) );
  // return rating.valueChanges();
  return this.afs.collection('Rating', ref => ref.where(
    'trainingID', '==', trainingID).where('userID', '==', user)).snapshotChanges().pipe(
    map(changes => {
      return changes.map(a => {
        const data = a.payload.doc.data() as Rating;
        return data;
      });
    })
    );
}

updateStar(rating, trainingID, userID) {
  this.ratingDoc = this.afs.doc(`Rating/${userID}_${trainingID}`);
  this.ratingDoc.set(rating);
}

// Create or update star
setStar(userID, userName, trainingID, value, comments) {
  // Star document data
  const timestamp = new Date();
  const star: Star = { userID, userName, trainingID, value , comments, timestamp};

  // Custom doc ID for relationship
  const starPath = `Rating/${star.userID}_${star.trainingID}`;

  // Set the data, return the promise
  return this.afs.doc(starPath).set(star);
}

}

