export interface Request {
  status: string;
  id?: string;
  dateEnd?: Date;
  dateStart?: Date;
  dateRequest?: any;
  location?: string;
  motivation?: string;
  comment?: string;
  coachComment?: string;
  currentProject?: string;
  courseLink?: URL;

  traineeCoach?: string;
  traineeCoachFirstName?: string;
  projectManager?: string;
  traineeDays?: number;
  traineeEmail?: string;
  traineeId?: string;
  traineeMoney?: number;
  traineeName?: string;
  traineeFirstName?: string;

  trainingCost?: number;
  trainingDuration?: number;
  trainingCostProject?: number;
  trainingDurationProject?: number;
    
  trainingId?: string;
  trainingName?: string;
  instituteName?: string;
  certifTitle?: string;
  rembourse?: string;

  dateBudget?:string;
}
