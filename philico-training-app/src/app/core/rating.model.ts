
export interface Rating {
  trainingID;
  userID;
  userName;
  value;
  comments;
  timestamp;
}
