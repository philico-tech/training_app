
import { Routes } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { UserComponent } from './user/user.component';
import { HeaderComponent } from './header/header.component';
import { TrainingFilterComponent } from './training-filter/training-filter.component';
import { TrainingRequestComponent } from './training-filter/training-request/training-request.component';
import { UserResolver } from './user/user.resolver';
import { AuthGuard } from './core/auth.guard';

export const rootRouterConfig: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent }, // , canActivate: [AuthGuard] },
  //  { path: 'user', component: UserComponent,  resolve: { data: UserResolver}},
  //  { path: 'trainings', component: TrainingsComponent},

  { path: 'header', component: HeaderComponent, canActivate: [AuthGuard],
  children: [
    { path: '',  redirectTo: 'user', pathMatch: 'full'},
    { path: 'user', component: UserComponent,  resolve: { data: UserResolver}},
    { path: 'trainings-filter', component: TrainingFilterComponent},
    { path: 'trainings-request/:id', component: TrainingRequestComponent},

  ]
},
];


