import { Component } from '@angular/core';
import { AuthService } from '../core/auth.service';
import { Router, Params } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as firebase from 'firebase/app';


@Component({
  selector: 'app-page-login',
  templateUrl: 'login.component.html',
  styleUrls: ['login.scss']
})
export class LoginComponent {

  loginForm: FormGroup;
  errorMessage = '';

  constructor(
    public authService: AuthService,
    private router: Router,
  ) {
  }


  tryGoogleLogin() {
    this.authService.doGoogleLogin()
    .then(res  => { if (firebase.auth().currentUser.email.endsWith('@philico.com')) {
      this.router.navigate(['/header']);
    } else {
      // popup window for the warning for a wrong e-mail login:
      alert('@philico.com address required!');
      this.router.navigate(['']);
    }
    });
  }

}
