import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import {AuthService} from '../core/auth.service';
import {Location} from '@angular/common';
import { UserService } from './../core/user.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  constructor(
    public authService: AuthService,
    private router: Router,
    private location: Location,
    private userService:UserService
  ) { }

  current_user:any;
  userFirstName: any;

  ngOnInit() {

    //   //  TEST
    // this.userService.get_fs_user().subscribe(
    //   current_user => {
    // this.current_user = current_user;

    // });
    this.userService.get_current_user().subscribe(
      current_user => {
        this.current_user = current_user;
        this.userFirstName = current_user.FirstName;
      }
    );

  }

  logout() {
    this.authService.doLogout()
      .then((res) => {
        this.router.navigate(['/login']);
      }, (error) => {
        console.log('Logout error', error);
      });
  }

}
