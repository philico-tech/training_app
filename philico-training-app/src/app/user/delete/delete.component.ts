import { Component,  OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder, FormGroup } from '@angular/forms';
// import {UserService} from '../../core/user.service';
import { Training } from '../../core/training.model';
import { TrainingService } from '../../core/training.service';
import { RequestService } from '../../core/request.service';
import { Trainee } from '../../core/trainee.model';
import { Request } from '../../core/request.model';
// e-mail
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment'; // for the endpoint
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import {formatDate} from '@angular/common';



@Component({
  selector: 'app-delete',
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.scss']
})


export class DeleteComponent {


request: Request={
  trainingName: '',
  trainingCost: 0,
  trainingDuration: 0,
  dateStart: null,
  traineeName: '',
  traineeCoach: '',
  status: '',
  comment:''
};


updateData: AngularFirestoreCollection<any> = this.afs.collection('Requests');

  constructor(
    public trainingService: TrainingService,
    public requestService: RequestService,
    private afs: AngularFirestore,
    private fb: FormBuilder,
    private http: HttpClient,
    public dialogRef: MatDialogRef<DeleteComponent>,
    @Inject(MAT_DIALOG_DATA) public data) {

    this.request = data.element;
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

    deleteRequest(element) :void{
      const endpoint = environment.fbCloudFunHttpEmailEndpoint;
      const data = {
        toEmail:this.request.traineeCoach,
        toFirstName: this.request.traineeCoachFirstName,
        fromEmail: this.request.traineeCoach,
        subject: 'Training App Notification: The request for ' + this.request.trainingName + ' has been deleted by '+ this.request.traineeFirstName,
        message:  this.request.traineeFirstName + ' deleted the request for the course "' + this.request.trainingName + '"',
      };
      this.http.post(endpoint, data).subscribe();
	    this.updateData.doc(element.id).delete().then(() =>
	      console.log('The request was deleted')
	    )

	    this.dialogRef.close('saved');
    }
}
