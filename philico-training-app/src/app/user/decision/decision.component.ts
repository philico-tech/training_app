import { Component,  OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder, FormGroup } from '@angular/forms';
// import {UserService} from '../../core/user.service';
import { Training } from '../../core/training.model';
import { TrainingService } from '../../core/training.service';
import { RequestService } from '../../core/request.service';
import { UserService } from '../../core/user.service';
import { Trainee } from '../../core/trainee.model';
import { Request } from '../../core/request.model';
// e-mail
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment'; // for the endpoint
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import {formatDate} from '@angular/common';



@Component({
  selector: 'app-decision',
  templateUrl: './decision.component.html',
  styleUrls: ['./decision.component.scss']
})
export class DecisionComponent {

	// ***************** for e-mail **********************************
	// endpoint = 'https://us-central1-philico-training-app.cloudfunctions.net/httpEmail';
	// ***************************************************************

  userEmail = sessionStorage.getItem('email');

  userRequest;
  userRequestArray;

	decision: string;
	status: string;

	request: Request={
	  trainingName: '',
	  trainingCost: 0,
	  trainingDuration: 0,
	  dateStart: null,
	  traineeName: '',
	  traineeCoach: '',
	  status: '',
	  comment:''
	};

	updateData: AngularFirestoreCollection<any> = this.afs.collection('Requests');

	constructor(
      public trainingService: TrainingService,
      public userService: UserService,
	    private afs: AngularFirestore,
	    public requestService: RequestService,
	    private fb: FormBuilder,
	    private http: HttpClient,
	    public dialogRef: MatDialogRef<DecisionComponent>,
	@Inject(MAT_DIALOG_DATA) public data) {
		this.decision = data.decisionRequest;
	    this.request = data.element;

	    if (this.decision === 'Approve') {
	      this.status = 'Approved';
	    } else {
	      this.status = 'Declined';
      }

      this.afs.collection('Users').doc(data.element.traineeEmail).valueChanges().subscribe( currentUser => {
        this.userRequest = currentUser;
      })

      this.afs.collection('Requests', ref => ref.where(
        'traineeEmail', '==', data.element.traineeEmail)).valueChanges().subscribe(
      userRequestArray => {
      this.userRequestArray = userRequestArray;
    })

	}

	onNoClick(): void {
	    this.dialogRef.close();
	}

	notifyDecision(element) {
    let decision = '';
    let message = '';
    const endpoint = environment.fbCloudFunHttpEmailEndpoint;
    if (this.status === 'Approved') {
      decision = 'approved';
      message = 'Your request for "' + this.request.trainingName + '" has been approved!'
    }
    else {
      decision = 'rejected';
      message = 'Unfortunately, your request for ' + this.request.trainingName + ' has been rejected. Please contact your coach for more information.'
    }
	   const data = {
	    toEmail:this.request.traineeEmail,
        toFirstName: this.request.traineeFirstName,
        fromEmail: this.request.traineeCoach,
        subject: 'Training App Notification: Your training request for ' + this.request.trainingName + ' has been ' + decision,
        message: message,
	    };


	   if (element.coachComment=== undefined) {
	  		this.updateData.doc(element.id).update({
		      coachComment: '',
		      status:  this.status}).then(()=>{
		    })
		} else if (element.coachComment !== '') {
		   this.updateData.doc(element.id).update({
        coachComment: element.coachComment,
		      status:  this.status}).then(()=>{
			});
		} else {
		 	this.updateData.doc(element.id).update({
        coachComment: '',
		      status:  this.status}).then(()=>{
			})
    }
    console.log('call user service update budget');
    this.userService.updateBudget(this.userRequest, this.userRequestArray);
	    this.dialogRef.close('saved');

	     this.http.post(endpoint, data).subscribe();
	}
}


