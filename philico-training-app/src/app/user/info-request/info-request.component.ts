import { Component,  OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder, FormGroup } from '@angular/forms';
// import {UserService} from '../../core/user.service';
import { Training } from '../../core/training.model';
import { TrainingService } from '../../core/training.service';
import { RequestService } from '../../core/request.service';
import { Trainee } from '../../core/trainee.model';
import { Request } from '../../core/request.model';
// e-mail
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment'; // for the endpoint
import {formatDate} from '@angular/common';




@Component({
  selector: 'app-info-request',
  templateUrl: './info-request.component.html',
  styleUrls: ['./info-request.component.scss']
})
export class InfoRequestComponent {


request: Request={
  trainingName: '',
  trainingCost: 0,
  trainingDuration: 0,
  dateStart: null,
  traineeName: '',
  traineeCoach: '',
  status: '',
  comment:''
};



  constructor(
    public trainingService: TrainingService,
    public requestService: RequestService,
    private fb: FormBuilder,
    private http: HttpClient,
    public dialogRef: MatDialogRef<InfoRequestComponent>,
    @Inject(MAT_DIALOG_DATA) public data) {

    this.request = data.element;
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}

