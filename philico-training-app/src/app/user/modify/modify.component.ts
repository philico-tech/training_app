import { Component,  OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder, FormGroup } from '@angular/forms';
// import {UserService} from '../../core/user.service';
import { Training } from '../../core/training.model';
import { TrainingService } from '../../core/training.service';
import { RequestService } from '../../core/request.service';
import { Trainee } from '../../core/trainee.model';
import { Request } from '../../core/request.model';
// e-mail
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment'; // for the endpoint




@Component({
  selector: 'app-modify',
  templateUrl: './modify.component.html',
  styleUrls: ['./modify.component.scss']
})
export class ModifyComponent {


request: Request={
  trainingName: '',
  trainingCost: 0,
  trainingDuration: 0,
  dateStart: null,
  traineeName: '',
  traineeCoach: '',
  status: '',
  comment:''
};

 LimitErrorCost = false;
 LimitErrorDuration = false;

dateCheckS:boolean;
dateCheckE:boolean;


  constructor(
    public trainingService: TrainingService,
    public requestService: RequestService,
    private fb: FormBuilder,
    private http: HttpClient,
    public dialogRef: MatDialogRef<ModifyComponent>,
    @Inject(MAT_DIALOG_DATA) public data) {

    this.request = data.element;
  }

   checkLimit(number, type){
    if(type == 'Cost'){
      if(number < 0){
        this.LimitErrorCost = true;
        return true
      }if(number > 7500){
        this.LimitErrorCost = true;
        return true
      }else{
        this.LimitErrorCost = false;
        return false
      }

    }if(type == 'Duration'){
      if(number < 0){
        this.LimitErrorDuration = true;
        return true
      }if(number > 15){
        this.LimitErrorDuration = true;
        return true
      }else{
        this.LimitErrorDuration = false;
        return false
      }
    }
  }

  compareStartDate(start, end){
    if(end < start){
      // console.log('Start:---------' +'check function- Start: '+ start + '  End: ' + end + '  ');
      this.dateCheckS = true;
      return 0;
    }else{
      // console.log('Start:+++++++++' +'check function- Start: '+ start + '  End: ' + end + '  ');
      this.dateCheckS = false;
      return 0;
    }
  }


  compareEndDate(start, end){
    if(end < start){
      // console.log('End:---------'+'check function- End: '+ start + '  End: ' + end + '  ');
      this.dateCheckE = true;
      return 0;
    }else{
      // console.log('End:+++++++++'+'check function- End: '+ start + '  End: ' + end + '  ');
      this.dateCheckE = false;
      return 0;
    }
  }

  close(): void {
    this.dialogRef.close();
  }

  save() {
      const endpoint = environment.fbCloudFunHttpEmailEndpoint;
      console.log(endpoint);


      const data = {
        toEmail:this.request.traineeCoach,
        toFirstName: this.request.traineeCoachFirstName,
        fromEmail: this.request.traineeCoach,
        subject: 'Training App Notification: The request ' + this.request.trainingName + ' has been modified by ' + this.request.traineeFirstName,
        message:  this.request.traineeFirstName + ' modified the pending request for the course "' + this.request.trainingName + '".',
      };

    this.requestService.updateRequest(this.request, this.request.id);
    this.http.post(endpoint, data).subscribe();
    this.dialogRef.close('saved');
  }

}

