import { Component, ViewChild, OnInit, Inject } from '@angular/core';
import { ElementRef, AfterViewInit } from "@angular/core";

import { AuthService } from '../core/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FirebaseUserModel } from '../core/user.model';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument  } from '@angular/fire/firestore';
import { Observable, of } from 'rxjs';
import { Trainee } from './../core/trainee.model';
import { Training } from './../core/training.model';
import { Request } from './../core/request.model';
import { UserService } from './../core/user.service';
import { RequestService } from './../core/request.service';
import { ToastrService } from 'ngx-toastr';
import { InfoRequestComponent } from './info-request/info-request.component';
import { ModifyComponent  } from './modify/modify.component';
import { CancelComponent  } from './cancel/cancel.component';
import { DecisionComponent } from './decision/decision.component';
import { DeleteComponent } from './delete/delete.component';

// Angular Material 2
import { MatDialogConfig, MatTableDataSource} from '@angular/material';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { MatSort } from '@angular/material/sort';


import { DataSource } from '@angular/cdk/collections';
import { animate, state, style, transition, trigger} from '@angular/animations';
import { Chart } from 'chart.js';
// rxjs
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { map,  switchMap } from 'rxjs/operators';
import * as _ from 'lodash';

// e-mail
import { HttpClient } from '@angular/common/http';
import { environment } from './../../environments/environment'; // for the endpoint
import {formatDate} from '@angular/common';


@Component({
  selector: 'app-page-user',
  templateUrl: 'user.component.html',
  styleUrls: ['user.scss'],
  animations:  [
  trigger('detailExpand', [
    state('collapsed', style({ height: '0px', minHeight: '0', visibility: 'hidden' })),
    state('expanded', style({ height: '*', visibility: 'visible' })),
    transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
  
})

export class UserComponent implements OnInit {

  user: FirebaseUserModel = new FirebaseUserModel();
  profileForm: FormGroup;


  //-------------------------------------------------------
  ///unwrapped arrays from firebase
  requestsA: any;
  filteredRequestA: any;

    ///filter-able properties
  status: string;
  nameTrainee: string;
  Role: string;
  traineeEmail: string;
  traineeCoach: string;
  hiddenButton = true;

  ///Active filter
  filtersA = {};

  //-------------------------------------------------------

  userRequestArray: any;
  email: string;
  aux = sessionStorage.getItem('email');

  budgetCalculation =[];
  startCalculation;


  displayedColumnsT  = ['Training Name', 'Training Duration', 'Training Cost', 'Start Date', 'End Date' , 'Date Request', 'Project Manager', 'Coach', 'Status', 'Action1']; //NEW
  // displayedColumnsT  = ['Training Name', 'Date Request', 'Project Manager', 'Training Cost','Start Date', 'Training Duration','Coach','Status', 'Info', 'Delete']; //OLD
  displayedColumnsC  = ['Training Name', 'Training Duration', 'Training Cost', 'Start Date', 'Date Request', 'Trainee', 'Project Manager', 'Coach', 'Status', 'Action'];
  // old displayedColumnsC  = ['Training Name', 'Date Request', 'Project Manager', 'Training Cost','Start Date', 'Training Duration', 'Trainee','Coach','Status', 'Info', 'Decision', 'Cancel'];
  displayedColumnsB  = ['Trainee Name', 'Days Left', 'Money Left']; //NEW

 // 			Coach	Status	Action

  // updateData: AngularFirestoreCollection<any> = this.afs.collection('Requests');
  userRequestData: AngularFirestoreCollection<any> = this.afs.collection('Requests', ref => ref.where('traineeEmail','==', this.aux
    ).where('status',      '==', 'Approved'
    ).where('dateBudget',  '==', formatDate(new Date(), 'yyyy', 'en')));
  userRequest = this.userRequestData.valueChanges();

  requestDatabase = new requestDatabase(this.requestService);
  // dataSource;
  dataSourceTraineeRequest: any;
  listCoachees: any;
  filterStatus: string;

  current_user: any;
  currentYear = formatDate(new Date(), 'yyyy', 'en');

  ///Charts
  BarChartM=[];
  PieChartM=[];
  BarChartT=[];
  PieChartT=[];
  // PieChartB=[];
  percentageMoneyCY=0;
  userMoneyLeft = 0;
  userDaysLeft = 0;
  percentageTimeCY=0;
  screen:Boolean;
  hidden:Boolean;
  isChecked:boolean;
  startPlots;

  dataSourceSort:any;

  stopPropagation(event){
    event.stopPropagation();
} 

@ViewChild(MatSort) sort: MatSort;


  constructor(
    public authService: AuthService,
    private route: ActivatedRoute,
    private location: Location,
    private fb: FormBuilder,
    private router: Router,
    public afs: AngularFirestore,
    public dialog: MatDialog,
    private http: HttpClient,
    private requestService: RequestService,
    private userService: UserService,
    private toastr: ToastrService,
    ) {
      this.isChecked = false;
    }


  ngOnInit() {

    const aux = sessionStorage.getItem('email');
    // console.log('email stored: ' + aux);

   this.userService.get_fs_user().subscribe(
      current_user => {
        this.current_user = current_user;
      });


    this.afs.collection('Users', ref => ref.where(
      'Coach', '==', aux)).valueChanges().subscribe(
    users => {
      this.listCoachees = users;
    })


      this.route.data.subscribe(routeData => {
        const data = routeData['data'];
        if (data) {
          this.user = data;
          this.createForm(this.user.name);
        }
      });

      // *****************
      // this.dataSource = new RequestDataSource(this.requestDatabase, this.sort);
      this.dataSourceTraineeRequest =  this.requestService.getRequestsTrainee(aux);
      //console.log('Test: ', this.dataSourceTraineeRequest);
      // *****************


      this.userService.getTraineeApprovedRequest().subscribe(
        userRequestArray => {
          this.userRequestArray = userRequestArray;
          this.startCalculation = this.getBudgetCalculation();
          this.startPlots = this.plots();
        })
//-----------------------------------"hidden = !hidden----------------------------------
 this.afs.collection('/Requests').snapshotChanges().pipe(
      map(changes => {
        return changes.map(a => {
          const data = a.payload.doc.data() as Request;
          data.id = a.payload.doc.id;
          return data;
        });
      })
      ).subscribe(requests => {
        this.requestsA = requests;
        this.applyFilters()
      })

   this.filterExact('traineeCoach', aux)

//---------------------------------------------------------------------
 
    }

//---------------------------------------------------------------------


    private applyFilters(){
      this.filteredRequestA = _.filter(this.requestsA, _.conforms(this.filtersA));
      this.dataSourceSort = new MatTableDataSource(this.filteredRequestA);
      this.dataSourceSort.sort = this.sort;

    }

    ///filter property by equality to rule
    filterExact(property: string, rule: any){
      this.filtersA[property] = val => val == rule
      this.applyFilters()
    }


    /// removes filter
    removeFilter(property: string) {
      delete this.filtersA[property]
      this[property] = undefined
      this.applyFilters()
    }

    checkBoxHandler(option: string){
      if(option === 'toggle'){
      this.isChecked = !this.isChecked;
      if(this.isChecked === true){
        this.removeFilter('traineeCoach');
      }  
      else {
        this.filterExact('traineeCoach', this.user.email);
      };
    }else{
      this.isChecked = false;
    };
  

    }




//---------------------------------------------------------------------

    plots(){

      // pie chart:
      this.PieChartM = new Chart('pieChartM', {
        type: 'doughnut',
        data: {
          labels: ["Available", "Pending", "Used"],
          // labels: ["Available",  "Used"],
          datasets: [{
            data: [this.startCalculation[Number(this.startCalculation.length)-2].moneyBudget,
            this.startCalculation[Number(this.startCalculation.length)-2].pendingMoney,
            this.startCalculation[Number(this.startCalculation.length)-2].totalMYear],
            // backgroundColor: [],  //this.startCalculation[]
            // fontColor: 'White',
            backgroundColor: [
            'rgba(1,65,111,1)',
            'rgba(1,65,111,0.75)',
            'rgba(1,65,111,0.5)',],
            borderWidth: 1,
          }]
        },
        options: {
          tooltips: {
            callbacks: {
              title: function (tooltipItem, data) { return data.labels[tooltipItem[0].index]; },
              label: function (tooltipItem, data) {
                var amount = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                return amount + ' CHF ';
              },
            }
          },
          cutoutPercentage:60,
          legend: {
            position: 'left',
            display: true,
            labels: {
              fontColor: 'rgba(92, 92, 92, 1)',
              fontSize: 16
          }
          },
        }
      });

      this.PieChartT = new Chart('pieChartT', {
        type: 'doughnut',
        data: {
          labels: ["Available", "Pending", "Used"],
          // labels: ["Available",  "Used"],
          datasets: [{
            label: 'Budget of the year',
            data: [this.startCalculation[Number(this.startCalculation.length)-2].timeBudget,
            this.startCalculation[Number(this.startCalculation.length)-2].pendingTime,
            this.startCalculation[Number(this.startCalculation.length)-2].totalTYear],  //this.startCalculation[]
            backgroundColor: [
              'rgba(1,65,111,1)',
              'rgba(1,65,111,0.75)',
              'rgba(1,65,111,0.5)',],
            borderWidth: 1,
          }]
        },
        options: {
          labels: {
            fontColor: 'rgba(92, 92, 92, 1)',
          },
          tooltips: {
            callbacks: {
              title: function (tooltipItem, data) { return data.labels[tooltipItem[0].index]; },
              label: function (tooltipItem, data) {
                var amount = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                return amount + ' Days ';
              },
            }
          },
          cutoutPercentage:60,
          legend: {
            position: 'left',
            display: true,
            labels: {
              fontColor: 'rgba(92, 92, 92, 1)',
              fontSize: 16
          }
          },
        }
      });

      // Bar chart:
      this.BarChartM = new Chart('barChartM', {
        // type: 'line',
        type: 'bar',
        data: {
          labels: [
          this.startCalculation[Number(this.startCalculation.length)-4].year,
          this.startCalculation[Number(this.startCalculation.length)-3].year,
          this.startCalculation[Number(this.startCalculation.length)-2].year
          ],
          datasets: [{
            label:'Used money budget',/* {   
            text: 'Overview Remaining Money Budget',
            fontStyle: 'bold',
            },*/
            data: [
            this.startCalculation[Number(this.startCalculation.length)-4].totalMYear.toFixed(),
            this.startCalculation[Number(this.startCalculation.length)-3].totalMYear.toFixed(),
            this.startCalculation[Number(this.startCalculation.length)-2].totalMYear.toFixed()
            ],
            backgroundColor: 'rgba(1,65,111,1)',
            // fill: false,
            borderWidth: 3,
            borderColor: 'rgba(1,65,111,0.5)',
          }]
        },
        options: {
          legend: {
            labels: {
              fontColor: 'rgba(92, 92, 92, 1)',
              fontSize: 16
          }
          },
          labels: {
            fontStyle: 'bold',
            fontColor: 'rgba(92, 92, 92, 1)',
          },
          tooltips: {
            callbacks: {
              label: (item) => `${item.yLabel} CHF`, //Please comment old code instead of deleting it ;)
            },
          },

          scales: {
            yAxes: [{
              ticks: {
                beginAtZero:false,
                fontStyle: 'bold',
                fontColor: 'rgba(92, 92, 92, 1)',
                min: 0,
                max: 7500,
                stepSize: 2500,
              }
            }],
            xAxes: [{
              ticks: {
                fontStyle: 'bold',
                fontColor: 'rgba(92, 92, 92, 1)',
              }
          }]
          }
        }
      });

      this.BarChartT = new Chart('barChartT', {
        // type: 'line',
        type: 'bar',
        data: {
          labels: [
          this.startCalculation[Number(this.startCalculation.length)-4].year,
          this.startCalculation[Number(this.startCalculation.length)-3].year,
          this.startCalculation[Number(this.startCalculation.length)-2].year
          ],
          datasets: [{
            label: 'Used time budget',
/*             {text: 'Overview Remaining Time Budget',
             fontStyle: 'bold'},*/
            data: [
            this.startCalculation[Number(this.startCalculation.length)-4].totalTYear.toFixed(),
            this.startCalculation[Number(this.startCalculation.length)-3].totalTYear.toFixed(),
            this.startCalculation[Number(this.startCalculation.length)-2].totalTYear.toFixed()
            ],
            backgroundColor: 'rgba(1,65,111,1)',
            // fill: false,
            borderWidth: 3,
            borderColor: 'rgba(1,65,111,0.5)',
          }]
        },
        options: {
          legend: {
            labels: {
              fontColor: 'rgba(92, 92, 92, 1)',
              fontSize: 16
          }
          },
          labels: {
            fontColor: 'rgba(92, 92, 92, 1)',
          },
          tooltips: {
            callbacks: {
              label: (item) => `${item.yLabel} Days`,
            },
          },

          scales: {
            yAxes: [{
              ticks: {
                beginAtZero:false,
                fontStyle: 'bold',
                fontColor: 'rgba(92, 92, 92, 1)',
                min: 0,
                max: 15,
                stepSize: 5,
              }
            }],
            xAxes: [{
              ticks: {
                fontStyle: 'bold',
                fontColor: 'rgba(92, 92, 92, 1)',
              }
          }]
          }
        }
      });
      return true
    };

    pieCenter(){
      //console.log(window.innerWidth);
      return window.innerWidth >= 1197;
    };



    getBudgetCalculation() {

      const currentBudget = this.userService.updateBudget(this.current_user[0], this.userRequestArray);
      this.percentageMoneyCY = currentBudget.currentYearMoney;
      this.percentageTimeCY = currentBudget.currentYearDay;
      this.budgetCalculation = currentBudget.currentArray;
      this.userMoneyLeft = this.current_user[0].budgetLeftMoney;
      this.userDaysLeft = this.current_user[0].budgetLeftDays;

      return currentBudget.currentArray;
    }

    currentYearCheck(element){
      let sameYear = true;
      if (element.year == this.currentYear){
        // console.log('Test Current Year IF');
        sameYear = true;
      }else{
        // console.log('Test Current Year ELSE');
        sameYear =false;
      };
      return sameYear;
    }


    showDecision(status){
      let showDec = false;
      if(status == 'Approved'){
        // console.log('Test show Decision');
        showDec = false;
      }else if(status == 'Declined'){
        // console.log('Test show Decision2');
        showDec = false;
      } else {
        // console.log('Test show Decision3');
        showDec = true;
      };
      return showDec;
    }

    approvedRequest(status){
      let appRe = false;
      if(status =='Approved'){
        appRe = true;
        // console.log('Test approved Request');
      } else {
        appRe = false;
        // console.log('Test approved Request');
      };
      return appRe;
    }


    approved(status){
      let approved = false;
      if(status =='Approved'){
        // console.log('Test approved');
        approved = false;
      } else {
        approved = true;
        // console.log('Test approved');
      };
      return approved;
    }


    createForm(name) {
      this.profileForm = this.fb.group({
        name: [name, Validators.required ]
      });
    }


    logout() {
      this.authService.doLogout()
      .then((res) => {
        this.router.navigate(['/login']);
      }, (error) => {
        console.log('Logout error', error);
      });
    }





    openDialog(element):void {

      const dialogConfig = new MatDialogConfig();


      dialogConfig.disableClose = false;
      dialogConfig.autoFocus = true;

      dialogConfig.data = {
        element : element,
      };

      dialogConfig.width ='1000px';

      // this.dialog.open(TrainingRequestComponent, dialogConfig);

      const dialogRef = this.dialog.open(InfoRequestComponent, dialogConfig);

      dialogRef.afterClosed().subscribe(result => {
        console.log('The dialog was closed');
      });
    }


    modifyOpenDialog(element):void {

      const dialogConfig = new MatDialogConfig();

      const message = 'Request modified';
      dialogConfig.disableClose = false;
      dialogConfig.autoFocus = true;

      dialogConfig.data = {
        element : element,
      };

      dialogConfig.width ='1000px';

      // this.dialog.open(TrainingRequestComponent, dialogConfig);

      const dialogRef = this.dialog.open(ModifyComponent, dialogConfig);

      dialogRef.afterClosed().subscribe(result => {
        if (result === 'saved') {
          this.toastr.success(message, '' , {
            positionClass: 'toast-bottom-center',
            timeOut: 3000
          })};
          console.log('The dialog was closed');
        });
    }


    cancelOpenDialog(element):void {

      const dialogConfig = new MatDialogConfig();

      const message = 'Request canceled';
      dialogConfig.disableClose = false;
      dialogConfig.autoFocus = true;

      dialogConfig.data = {
        element : element,
      };

      dialogConfig.width ='1000px';

      // this.dialog.open(TrainingRequestComponent, dialogConfig);

      const dialogRef = this.dialog.open(CancelComponent, dialogConfig);

      dialogRef.afterClosed().subscribe(result => {
        if (result === 'saved') {
          this.toastr.success(message, '' , {
            positionClass: 'toast-bottom-center',
            timeOut: 3000
          })};
          console.log('The dialog was closed');
        });
    }

    decisionOpenDialog(element, decision):void {

      const dialogConfig = new MatDialogConfig();
      const message = 'Request ' + decision.decision + 'd';
      dialogConfig.disableClose = false;
      dialogConfig.autoFocus = true;

      dialogConfig.data = {
        element : element,
        decisionRequest: decision.decision,
      };

      dialogConfig.width ='1000px';

      // this.dialog.open(TrainingRequestComponent, dialogConfig);

      const dialogRef = this.dialog.open(DecisionComponent, dialogConfig);

      dialogRef.afterClosed().subscribe(result => {
        if (result === 'saved') {
          this.toastr.success(message, '' , {
            positionClass: 'toast-bottom-center',
            timeOut: 3000
          });
        }
      }
      );
    }



    deleteOpenDialog(element):void {

      const dialogConfig = new MatDialogConfig();
      const message = 'The request has been deleted';
      dialogConfig.disableClose = false;
      dialogConfig.autoFocus = true;

      dialogConfig.data = {
        element : element,
      };

      dialogConfig.width ='400px';

      // this.dialog.open(TrainingRequestComponent, dialogConfig);

      const dialogRef = this.dialog.open(DeleteComponent, dialogConfig);

      dialogRef.afterClosed().subscribe(result => {
        //Not
        if (result === 'saved') {
          this.toastr.success(message, '' , {
            positionClass: 'toast-bottom-center',
            timeOut: 3000
          })};
          console.log('The dialog was closed');
        });
    }

  }

  export class requestDatabase {

    requestList = new BehaviorSubject([]);
    get data() {return this.requestList.value};

    constructor(private requestService: RequestService){
      this.requestService.getRequests().subscribe((requestService) => {this.requestList.next(requestService);})
    }
  }



  // export class RequestDataSource extends DataSource<any> {

  //   constructor(private requestDB: requestDatabase , private sort: MatSort ) {
  //     super()
  //   }

  //   connect():Observable<any> {
  //     const requestData = [
  //     this.requestDB.requestList,
  //     this.sort.sortChange
  //     ];

  //     return Observable.merge(...requestData).map(() => {
  //       return this.getSortedData();
  //     })

  //   }

  //   disconnect(){

  //   }

  //   getSortedData() {
  //     const data = this.requestDB.data.slice();
  //     if (!this.sort.active || this.sort.direction === '') { return data; };

  //     return data.sort((a, b) => {
  //       let propertyA: number|string = '';
  //       let propertyB: number|string = '';

  //       switch (this.sort.active) {
  //         case 'Training Name': [propertyA, propertyB] = [a.trainingName, b.trainingName]; break;
  //         case 'Training Cost': [propertyA, propertyB] = [a.trainingCost, b.trainingCost]; break;
  //         case 'Start Date': [propertyA, propertyB] = [a.dateStart, b.dateStart]; break;
  //         case 'Training Duration': [propertyA, propertyB] = [a.dateStart, b.dateStart]; break;
  //         case 'Trainee': [propertyA, propertyB] = [a.traineeName, b.traineeName]; break;
  //         case 'Coach': [propertyA, propertyB] = [a.traineeCoach, b.traineeCoach]; break;
  //         case 'Status': [propertyA, propertyB] = [a.status, b.status]; break;
  //       }

  //       let valueA = isNaN(+propertyA) ? propertyA : +propertyA;
  //       let valueB = isNaN(+propertyB) ? propertyB : +propertyB;

  //       return (valueA < valueB ? -1 : 1) * (this.sort.direction == 'asc' ? 1 : -1);
  //     });
  //   }
  // }




  // export class FilteredDataSource extends DataSource<any> {

  //   constructor(private inputobs) {
  //     super()
  //   }

  //   connect():Observable<any> {
  //     return this.inputobs;
  //   }

  //   disconnect(){
  //   }

  // }



        // case 'trainingName': return compare(a.trainingName, b.trainingName, isAsc);
        // case 'dateRequest': return compare(a.dateRequest, b.dateRequest, isAsc);
        // case 'projectManager': return compare(a.projectManager, b.projectManager, isAsc);
        // case 'trainingCost': return compare(a.trainingCost, b.trainingCost, isAsc);
        // // case 'dateStart': return compare(a.dateStart, b.dateStart, isAsc);
        // case 'trainingDurationProject': return compare(a.trainingDurationProject, b.trainingDurationProject, isAsc);
        // case 'traineeFirstName': return compare(a.traineeFirstName, b.traineeFirstName, isAsc);
        // case 'traineeCoachFirstName': return compare(a.traineeCoachFirstName, b.traineeCoachFirstName, isAsc);
        // case 'status': return compare(a.status, b.status, isAsc);