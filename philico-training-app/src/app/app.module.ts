
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule, MatIconModule, MatSortModule, MatSort, MatDialogModule, MatFormFieldModule} from '@angular/material';
// import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { MatTableModule, MatInputModule, MatSelectModule, MatTooltipModule} from '@angular/material';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { rootRouterConfig } from './app.routes';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { environment } from '../environments/environment';
import { LoginComponent } from './login/login.component';
import { UserComponent } from './user/user.component';
import { UserResolver } from './user/user.resolver';
import { AuthGuard } from './core/auth.guard';
import { AuthService } from './core/auth.service';
import { UserService } from './core/user.service';
import { TrainingService } from './core/training.service';
import { RequestService } from './core/request.service';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { NavbarComponent } from './navbar/navbar.component';
import { TrainingRequestComponent } from './training-filter/training-request/training-request.component';
import { TrainingFilterComponent } from './training-filter/training-filter.component';
import { TrainingModifyComponent } from './training-filter/training-modify/training-modify.component';
import { TrainingAddComponent } from './training-filter/training-add/training-add.component';
import { ToastrModule } from 'ngx-toastr';
import { InfoRequestComponent } from './user/info-request/info-request.component';
import { DecisionComponent } from './user/decision/decision.component';
import { DeleteComponent } from './user/delete/delete.component';
import { FilterCoursePipe } from './training-filter/filter-course.pipe';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatMenuModule} from '@angular/material/menu';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ModifyComponent } from './user/modify/modify.component';
import { TrainingDeleteComponent } from './training-filter/training-delete/training-delete.component';
import { CancelComponent } from './user/cancel/cancel.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    UserComponent,
    HeaderComponent,
    NavbarComponent,
    TrainingRequestComponent,
    TrainingFilterComponent,
    TrainingModifyComponent,
    TrainingAddComponent,
    InfoRequestComponent,
    DecisionComponent,
    DeleteComponent,
    FilterCoursePipe,
    ModifyComponent,
    TrainingDeleteComponent,
    CancelComponent,
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(), // ToastrModule added
    FormsModule,
    NgbModule,
    MatButtonToggleModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatExpansionModule,
    MatIconModule,
    MatMenuModule,
    MatButtonModule,
    MatTableModule,
    MatCheckboxModule,
    MatInputModule,
    MatSortModule,
    MatSelectModule,
    MatTooltipModule,
    // RouterModule.forRoot(rootRouterConfig, { useHash: false }),
    RouterModule.forRoot(rootRouterConfig, { onSameUrlNavigation: 'ignore' }),
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule, // imports firebase/firestore, only needed for database features
    AngularFireAuthModule, // imports firebase/auth, only needed for auth features
    MatDialogModule,
    MatInputModule,
    BrowserAnimationsModule,
  ],
  bootstrap: [AppComponent
  ],

  entryComponents: [TrainingRequestComponent,
    TrainingModifyComponent,
    TrainingAddComponent,
    InfoRequestComponent,
    DecisionComponent,
    DeleteComponent,
    ModifyComponent,
    TrainingDeleteComponent,
    CancelComponent,


  ],
  providers: [AuthService,
    UserService,
    MatSort,
    TrainingService,
    UserResolver,
    AuthGuard,
    RequestService]

})
export class AppModule { }
