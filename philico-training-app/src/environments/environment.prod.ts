export const environment = {
  production: true,
  fbCloudFunHttpEmailEndpoint: 'https://us-central1-philico-training-app.cloudfunctions.net/httpEmail',
  firebase: {
    apiKey: 'AIzaSyB0WNDc22wEwjnYqNWmE7_sw3g8oVLdA8E',
    authDomain: 'philico-training-app.firebaseapp.com',
    databaseURL: 'https://philico-training-app.firebaseio.com',
    projectId: 'philico-training-app',
    storageBucket: 'philico-training-app.appspot.com',
    messagingSenderId: '364397645999'
  }
};
