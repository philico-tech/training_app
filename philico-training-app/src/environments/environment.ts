// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  fbCloudFunHttpEmailEndpoint: 'https://us-central1-philico-training-app.cloudfunctions.net/httpEmail',
  firebase: {
    apiKey: 'AIzaSyDg1UWLX3KLDXGChtcM1oIxwC1TtwWpmcg',
    authDomain: 'philico-training-app-testing.firebaseapp.com',
    databaseURL: 'https://philico-training-app-testing.firebaseio.com',
    projectId: 'philico-training-app-testing',
    storageBucket: 'philico-training-app-testing.appspot.com',
    messagingSenderId: '77221041535'
  }
};


// export const environment = {
//   production: true,
//   fbCloudFunHttpEmailEndpoint: 'https://us-central1-philico-training-app.cloudfunctions.net/httpEmail',
//   firebase: {
//     apiKey: 'AIzaSyB0WNDc22wEwjnYqNWmE7_sw3g8oVLdA8E',
//     authDomain: 'philico-training-app.firebaseapp.com',
//     databaseURL: 'https://philico-training-app.firebaseio.com',
//     projectId: 'philico-training-app',
//     storageBucket: 'philico-training-app.appspot.com',
//     messagingSenderId: '364397645999'
//   }
// };

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
